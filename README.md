# Gametracker

Projeto que visa criar um organizador de progresso de jogos (em progresso e
terminados) multiplataforma em Java, usando a tecnologia JavaFX e banco de dados
local.

## Licença

GNU GLPv3
Copyright (c) 2016 José Volpato
