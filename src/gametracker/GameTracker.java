/**
 * GameTracker.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker;

import gametracker.controller.DashboardController;
import gametracker.controller.LoginController;
import gametracker.controller.GamePlatformAddEditController;
import gametracker.controller.GamePlayerAddEditController;
import gametracker.controller.MasterController;
import gametracker.controller.GamePlatformFindController;
import gametracker.controller.GamePlayerFindController;
import gametracker.controller.AddGameEntryController;
import gametracker.controller.UserFindEntryController;
import gametracker.controller.UserGraphsController;
import gametracker.controller.UserTodoListController;
import gametracker.util.ViewController;
import gametracker.model.GamePlayer;
import gametracker.util.GameTrackerDAO;
import gametracker.util.I18N;
import gametracker.util.LANGUAGE;
import java.util.Locale;
import java.util.Optional;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import gametracker.util.DynamicalyTranslatableViewController;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Button;

/**
 * Sistema de controle de listas de jogos.
 */
public class GameTracker extends Application
{
    private static GameTracker instance;

    private Stage stage;
    private Scene scene;
    private ViewController masterController;

    private Locale locale;

    private GamePlayer loggedPlayer;


    public GameTracker()
    {
        locale = Locale.getDefault();
        setUpLanguage();
    }

    /**
     * Retorna a instancia unica do programa.
     *
     * @return Instancia unica do programa.
     */
    public static GameTracker getInstance()
    {
        if (instance == null)
        {
            instance = new GameTracker();
        }
        return instance;
    }


    /**
     * Ponto de entrada da aplicacao.
     *
     * @param args Argumentos da entrada do programa.
     */
    public static void main(String[] args) { launch(args); }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        GameTracker.getInstance().stage = primaryStage;
        GameTracker.getInstance().stage.setResizable(false);
        GameTracker.getInstance().stage.setTitle(I18N.tr("apptitle"));
        GameTrackerDAO.getInstance().setUpDatabase();
        setMainView(new LoginController());
        GameTracker.getInstance().stage.show();
    }

    @Override
    public void stop() throws Exception
    {
        super.stop();
        doLogout();
        GameTrackerDAO.getInstance().closeDatabase();
    }

    /**
     * Realiza a rotina de validacao de login do programa.
     *
     * @param loggedPlayer
     */
    public void doLogin(GamePlayer loggedPlayer)
    {
        this.loggedPlayer = loggedPlayer;
        onLogin();
    }

    /**
     * Metodo executado quando o login e efetuado.
     */
    private void onLogin()
    {
        setMainView(new MasterController());
        setMasterViewContent(new DashboardController());
    }

    /**
     * Realiza o logout do usuario logado no sistema.
     */
    public void doLogout()
    {
        this.loggedPlayer = null;
        onLogout();
    }

    /**
     * Metodo executado quando o logout e efetuado.
     */
    private void onLogout()
    {
        setMainView(new LoginController());
    }

    /**
     * Retorna o ID do usuario logado no sistema.
     *
     * @return ID do usuario logado.
     */
    public GamePlayer getLoggedPlayer()
    {
        return this.loggedPlayer;
    }

    /**
     * Define o controlador de tela principal do programa.
     *
     * @param viewController Controlador de tela.
     */
    public void setMainView(ViewController viewController)
    {
        GameTracker.getInstance().masterController = viewController;
        this.scene = new Scene(GameTracker.getInstance().masterController.getParentNode());
        this.scene.getStylesheets().add("/gametracker/style/ClearWhite.css");
        GameTracker.getInstance().stage.setScene(this.scene);
    }

    /**
     * Define o controlador de conteudo da tela principal do programa.
     *
     * @param viewController Controlador de tela do conteudo.
     */
    public void setMasterViewContent(ViewController viewController)
    {
        if (this.masterController instanceof MasterController)
        {
            ((MasterController) this.masterController).
                    setMasterViewContent(viewController);
            if (viewController instanceof DashboardController)
            {
                ((MasterController) this.masterController).
                        setBackViewButtonVisible(false);
            }
            else
            {
                ((MasterController) this.masterController).
                        setBackViewButtonVisible(true);
            }
        }
    }

    /**
     * Exibe a dash de jogador na tela principal.
     */
    public void showUserDash()
    {
        if (this.masterController instanceof MasterController)
        {
            if ( ((MasterController) this.masterController).
                    getMasterViewController() instanceof DashboardController)
            {
                DashboardController dashboardController =
                        (DashboardController)
                            ((MasterController) this.masterController).
                                    getMasterViewController();

                dashboardController.showUserDash();
            }
        }
    }

    /**
     * Exibe a dash de publico na tela principal.
     */
    public void showPublicDash()
    {
        if (this.masterController instanceof MasterController)
        {
            if ( ((MasterController) this.masterController).
                    getMasterViewController() instanceof DashboardController)
            {
                DashboardController dashboardController =
                        (DashboardController)
                            ((MasterController) this.masterController).
                                    getMasterViewController();

                dashboardController.showPublicDash();
            }
        }
    }

    /**
     * Exibe a dash de master na tela principal.
     */
    public void showMasterDash()
    {
        if (this.masterController instanceof MasterController)
        {
            if ( ((MasterController) this.masterController).
                    getMasterViewController() instanceof DashboardController)
            {
                DashboardController dashboardController =
                        (DashboardController)
                            ((MasterController) this.masterController).
                                    getMasterViewController();

                dashboardController.showMasterDash();
            }
        }
    }

    /**
     * Mostra uma janela de dialogo de informacao.
     *
     * @param message Mensagem que sera mostrada na janela.
     */
    public void showInformationDialog(String message)
    {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(I18N.tr("InformationDialog.title"));
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Mostra uma janela de dialogo de alerta.
     *
     * @param message Mensagem que sera mostrada na janela.
     */
    public void showAlertDialog(String message)
    {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle(I18N.tr("AlertDialog.title"));
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Mostra uma janela de dialogo de erro.
     *
     * @param message Mensagem que sera mostrada na janela.
     */
    public void showErrorDialog(String message)
    {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(I18N.tr("ErrorDialog.title"));
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Mostra uma janela de dialogo de confirmacao de duas opcoes, OK e
     * Cancelar.
     *
     * @param message Mensagem que sera mostrada na janela.
     *
     * @return
     * true - A saida da janela pelo botao OK.
     * false - A saida da janela pelo botao Cancelar ou Fechar.
     */
    public boolean showConfirmationDialog(String message)
    {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(I18N.tr("ConfirmationDialog.title"));
        alert.setHeaderText(null);
        alert.setContentText(message);
        ((Button) alert.getDialogPane().
                lookupButton(ButtonType.OK)).setDefaultButton(false);
        ((Button) alert.getDialogPane().
                lookupButton(ButtonType.CANCEL)).setDefaultButton(true);
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    /**
     * Define o comportamento do botao de voltar da barra de controle de
     * janelas.
     */
    public void onBackPressed()
    {
        ViewController viewController =
                ((MasterController) this.masterController).
                        getMasterViewController();

        // Jogador
        // Adicionar entrada de jogo -> Jogador dash
        if (viewController instanceof AddGameEntryController)
        {
            setMasterViewContent(new DashboardController());
            showUserDash();
            return;
        }

        // Pesquisar entrada -> Jogador dash
        if (viewController instanceof UserFindEntryController)
        {
            setMasterViewContent(new DashboardController());
            showUserDash();
            return;
        }

        // Lista pendentes -> Jogador dash
        if (viewController instanceof UserTodoListController)
        {
            setMasterViewContent(new DashboardController());
            showUserDash();
            return;
        }

        // Graficos -> Jogador dash
        if (viewController instanceof UserGraphsController)
        {
            setMasterViewContent(new DashboardController());
            showUserDash();
            return;
        }

        // Publico


        // AREA MASTER
        // Tela de controle de usuario/plataforma -> Master dash
        if (viewController instanceof GamePlayerFindController ||
            viewController instanceof GamePlatformFindController)
        {
            setMasterViewContent(new DashboardController());
            showMasterDash();
            return;
        }

        // Tela de adicionar usuario -> Tela de controle de usuario
        if (viewController instanceof GamePlayerAddEditController)
        {
            setMasterViewContent(new GamePlayerFindController());
            return;
        }

        // Tela de adicionar plataforma -> Tela de controle de plataforma
        if (viewController instanceof GamePlatformAddEditController)
        {
            setMasterViewContent(new GamePlatformFindController());
        }
    }

    /**
     * Configura e adequa o locale padrao para uma das linguas disponiveis do
     * sistema.
     */
    private void setUpLanguage()
    {
        switch (locale.getLanguage())
        {
            case "pt":
                setLanguage(LANGUAGE.PORTUGUESE);
            break;

            default:
            case "en":
                setLanguage(LANGUAGE.ENGLISH);
            break;

            case "es":
                setLanguage(LANGUAGE.SPANISH);
            break;
        }
    }

    /**
     * Define a lingua a ser usada pelo programa.
     *
     * @param language Lingua a ser usada pelo programa.
     */
    public void setLanguage(LANGUAGE language)
    {
        switch (language)
        {
            case PORTUGUESE:
                locale = new Locale("pt", "BR");
            break;

            case ENGLISH:
                locale = new Locale("en", "US");
            break;

            case SPANISH:
                locale = new Locale("es", "AR");
            break;
        }

        if (masterController != null &&
            masterController instanceof DynamicalyTranslatableViewController)
        {
            ((DynamicalyTranslatableViewController) masterController).updateTranslation();
        }
    }

    /**
     * Retorna a lingua usada pelo programa.
     *
     * @return Lingua usada pelo programa.
     */
    public LANGUAGE getLanguage()
    {
        switch (locale.getLanguage())
        {
            case "pt":
                return LANGUAGE.PORTUGUESE;

            default:
            case "en":
                return LANGUAGE.ENGLISH;

            case "es":
                return LANGUAGE.SPANISH;
        }
    }

    /**
     * Retorna o locale definido no sistema.
     *
     * @return Locale definido no sistema.
     */
    public Locale getLocale()
    {
        return locale;
    }

    /**
     * Define o campo que recebera o foco na inicializacao da tela.
     *
     * @param node Campo que recebera o foco na inicializacao da tela.
     */
    public void setInitialFocus(Node node)
    {
        Platform.runLater(() -> { node.requestFocus(); });
    }
}
