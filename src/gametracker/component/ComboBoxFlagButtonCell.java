/**
 * ComboBoxFlagButtonCell.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.component;

import gametracker.util.LANGUAGE;
import gametracker.util.LanguageEnumConverter;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Define uma classe de celula customizada para lista de Combobox. Nessa celula,
 * a opcao e substituida por uma bandeira coorespondente.
 */
public class ComboBoxFlagButtonCell extends ListCell<String>
{
    @Override
    protected void updateItem(String item, boolean empty)
    {
        super.updateItem(item, empty);

        if (item == null || item.equals(""))
        {
            setGraphic(null);
            return;
        }

        LANGUAGE itemLanguage = LanguageEnumConverter.getInstance().convertToEnumValue(item);

        Image flagIcon = null;

        switch (itemLanguage)
        {
            case PORTUGUESE:
                flagIcon = new Image("/gametracker/resources/flag_br.png");
            break;

            case ENGLISH:
                flagIcon = new Image("/gametracker/resources/flag_us.png");
            break;

            case SPANISH:
                flagIcon = new Image("/gametracker/resources/flag_ar.png");
            break;
        }
        ImageView flag = new ImageView(flagIcon);
        setGraphic(flag);
    }
}
