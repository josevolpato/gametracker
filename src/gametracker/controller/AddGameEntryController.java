/**
 * UserAddEntryController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.GameTracker;
import gametracker.model.GAMEENTRYSTATUS;
import gametracker.model.GameEntry;
import gametracker.util.ViewController;
import gametracker.model.GameEntryStatusEnumConverter;
import gametracker.model.GamePlatform;
import gametracker.util.DAO_SAVE_RESULT;
import gametracker.util.GameTrackerDAO;
import gametracker.util.I18N;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 */
public final class AddGameEntryController extends ViewController
{
    private GameEntry gameEntry;

    @FXML private Label lblGameName;
    @FXML private TextField txtGameName;
    @FXML private Label lblGamePlatform;
    @FXML private ComboBox<GamePlatform> cbGamePlatform;
    @FXML private Label lblEntryStatus;
    @FXML private ComboBox<String> cbEntryStatus;
    @FXML private CheckBox chbStillPlaying;
    @FXML private Label lblComment;
    @FXML private TextArea txtaComment;
    @FXML private Button btnConfirm;


    public AddGameEntryController()
    {
        super(I18N.tr("AddGameEntryView.title.add"),
              "/gametracker/view/AddGameEntryView.fxml");

        ArrayList<GamePlatform> gamePlatforms = (ArrayList<GamePlatform>)
                GameTrackerDAO.getInstance().getAllGamePlatform();
        cbGamePlatform.getItems().addAll(gamePlatforms);

        lblGameName.setText(I18N.tr("AddGameEntryView.lblGameName"));
        lblGamePlatform.setText(I18N.tr("AddGameEntryView.lblGamePlatform"));
        lblEntryStatus.setText(I18N.tr("AddGameEntryView.lblEntryStatus"));
        chbStillPlaying.setText(I18N.tr("AddGameEntryView.chboxStillPlaying"));
        lblComment.setText(I18N.tr("AddGameEntryView.lblComment"));
        btnConfirm.setText(I18N.tr("AddGameEntryView.btnConfirm.add"));

        cbEntryStatus.getItems().addAll(
                GameEntryStatusEnumConverter.getInstance().getAllLiterals());

        btnConfirm.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event)
            {
                addGameEntry();
            }

        });

        gameEntry = new GameEntry();
    }

    public AddGameEntryController(GameEntry gameEntry)
    {
        super(I18N.tr("AddGameEntryView.title.edit"),
              "/gametracker/view/AddGameEntryView.fxml");

        ArrayList<GamePlatform> gamePlatforms = (ArrayList<GamePlatform>)
                GameTrackerDAO.getInstance().getAllGamePlatform();
        if (gamePlatforms.size() > 0)
        {
            cbGamePlatform.getItems().addAll(gamePlatforms);
        }
        else
        {
            GameTracker.getInstance().showErrorDialog(I18N.tr("AddGameEntryView.dialog.alertNoGamePlatform"));
            GameTracker.getInstance().onBackPressed();
            return;
        }

        lblGameName.setText(I18N.tr("AddGameEntryView.lblGameName"));
        lblGamePlatform.setText(I18N.tr("AddGameEntryView.lblGamePlatform"));
        lblEntryStatus.setText(I18N.tr("AddGameEntryView.lblEntryStatus"));
        chbStillPlaying.setText(I18N.tr("AddGameEntryView.chboxStillPlaying"));
        lblComment.setText(I18N.tr("AddGameEntryView.lblComment"));
        btnConfirm.setText(I18N.tr("AddGameEntryView.btnConfirm.edit"));

        cbEntryStatus.getItems().addAll(
                GameEntryStatusEnumConverter.getInstance().getAllLiterals());

        btnConfirm.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event)
            {
                updateGameEntry();
            }

        });

        this.gameEntry = gameEntry;
        modelToView();
    }


    @Override
    protected boolean hasErrorsInForm()
    {
        if (txtGameName.getText() == null || txtGameName.getText().equals(""))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("AddGameEntryView.dialog.alertEmptyGameName"));
            return true;
        }

        if (cbGamePlatform.getSelectionModel().getSelectedItem() == null)
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("AddGameEntryView.dialog.alertEmptyGamePlatform"));
            return true;
        }

        if (cbEntryStatus.getSelectionModel().getSelectedItem() == null ||
            GameEntryStatusEnumConverter.getInstance().convertToEnumValue(
                cbEntryStatus.getSelectionModel().getSelectedItem()) ==
                GAMEENTRYSTATUS.INVALID)
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("AddGameEntryView.dialog.alertEmptyEntryStatus"));
            return true;
        }

        return false;
    }

    @Override
    protected void modelToView()
    {
        txtGameName.setText(gameEntry.getGameName());
        cbGamePlatform.getSelectionModel().select(gameEntry.getGamePlatform());
        chbStillPlaying.setSelected(gameEntry.isStillPlaying());
        cbEntryStatus.getSelectionModel().select(GameEntryStatusEnumConverter.getInstance().convertToLiteralValue(gameEntry.getEntryStatus()));
        txtaComment.setText(gameEntry.getComment());
    }

    @Override
    protected void viewToModel()
    {
        gameEntry.setGameName(txtGameName.getText());
        gameEntry.setGamePlatform(cbGamePlatform.getSelectionModel().getSelectedItem());
        gameEntry.setStillPlaying(chbStillPlaying.isSelected());
        gameEntry.setEntryStatus(GameEntryStatusEnumConverter.getInstance().convertToEnumValue(cbEntryStatus.getSelectionModel().getSelectedItem()));
        gameEntry.setGamePlayer(GameTracker.getInstance().getLoggedPlayer());
        gameEntry.setComment(txtaComment.getText());
    }

    private void addGameEntry()
    {
        if (hasErrorsInForm())
        {
            return;
        }

        viewToModel();
        DAO_SAVE_RESULT result = GameTrackerDAO.getInstance().saveGameEntry(gameEntry);
        switch (result)
        {
            case ID_ALREADY_ON_TABLE:
                GameTracker.getInstance().showAlertDialog(I18N.tr("AddGameEntryView.dialog.alertIdAlreadyOnTable"));
            break;

            case SUCCESS:
                GameTracker.getInstance().showInformationDialog(I18N.tr("AddGameEntryView.dialog.informationAddSuccess"));
            break;
        }
    }

    private void updateGameEntry()
    {
        if (hasErrorsInForm())
        {
            return;
        }

        viewToModel();
        GameTrackerDAO.getInstance().updateGameEntry(gameEntry);
    }
}
