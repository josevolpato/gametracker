package gametracker.controller;

import gametracker.GameTracker;
import gametracker.model.GAMEPLAYERPRIVILEGES;
import gametracker.model.GAMEPLAYERSTATUS;
import gametracker.model.GamePlayer;
import gametracker.util.GameTrackerDAO;
import gametracker.util.I18N;
import gametracker.util.ViewController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 *
 * @author jhcv
 */
public final class CreateAccountController extends ViewController
{
    private final GamePlayer newAccount;

    @FXML private Label lblEmail;
    @FXML private TextField txtEmail;
    @FXML private Label lblLogin;
    @FXML private TextField txtLogin;
    @FXML private Label lblPassword;
    @FXML private PasswordField ptxtPassword;
    @FXML private Label lblPasswordRepeat;
    @FXML private PasswordField ptxtPasswordRepeat;
    @FXML private Button btnConfirm;


    public CreateAccountController()
    {
        super(I18N.tr("CreateAccountView.title"),
                "/gametracker/view/CreateAccountView.fxml");

        lblEmail.setText(I18N.tr("CreateAccountView.lblEmail"));
        lblLogin.setText(I18N.tr("CreateAccountView.lblLogin"));
        lblPassword.setText(I18N.tr("CreateAccountView.lblPassword"));
        lblPasswordRepeat.setText(I18N.tr("CreateAccountView.lblPasswordRepeat"));
        btnConfirm.setText(I18N.tr("CreateAccountView.btnConfirm"));

        btnConfirm.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event)
            {
                createAccount();
            }

        });
        btnConfirm.setDefaultButton(true);

        newAccount = new GamePlayer();
    }


    @Override
    protected boolean hasErrorsInForm()
    {
        if (txtLogin.getText().equals(""))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("CreateAccountView.alertDialog.fillLoginField"));
            txtLogin.requestFocus();
            return true;
        }

        if (txtEmail.getText().equals(""))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("CreateAccountView.alertDialog.fillEmailField"));
            txtEmail.requestFocus();
            return true;
        }

        if (ptxtPassword.getText().equals(""))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("CreateAccountView.alertDialog.fillPasswordField"));
            ptxtPassword.requestFocus();
            return true;
        }

        if (ptxtPasswordRepeat.getText().equals(""))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("CreateAccountView.alertDialog.fillPasswordRepeatField"));
            ptxtPasswordRepeat.requestFocus();
            return true;
        }

        if (!ptxtPassword.getText().equals(ptxtPasswordRepeat.getText()))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("CreateAccountView.alertDialog.passwordMismatch"));
            ptxtPasswordRepeat.requestFocus();
            return true;
        }

        return false;
    }

    @Override
    protected void modelToView() { }

    @Override
    protected void viewToModel()
    {
        newAccount.setLogin(txtLogin.getText());
        newAccount.setEmail(txtEmail.getText());
        newAccount.setPassword(ptxtPassword.getText());
        newAccount.setPlayerPrivileges(GAMEPLAYERPRIVILEGES.USER);
        newAccount.setPlayerStatus(GAMEPLAYERSTATUS.WAITING_ACTIVATION);
    }

    private void createAccount()
    {
        if (hasErrorsInForm())
        {
            return;
        }

        viewToModel();
        
        if (GameTrackerDAO.getInstance().existsRepeatedGamePlayer(newAccount))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("CreateAccountView.alertDialog.accountAlreadyExists"));
            return;
        }
        
        GameTrackerDAO.getInstance().saveGamePlayer(newAccount);
        GameTracker.getInstance().showInformationDialog(I18N.tr("CreateAccountView.informationDialog.success"));
    }

}
