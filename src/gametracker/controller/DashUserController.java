/**
 * DashUserController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.util.ViewController;
import gametracker.GameTracker;
import gametracker.model.GAMEPLAYERPRIVILEGES;
import gametracker.util.GameTrackerDAO;
import gametracker.util.I18N;
import javafx.fxml.FXML;
import javafx.scene.control.Button;


public class DashUserController extends ViewController
{
    @FXML private Button btnUserAddEntry;
    @FXML private Button btnUserFindEntry;
    @FXML private Button btnUserTodoList;
    @FXML private Button btnUserGraphs;


    public DashUserController()
    {
        super("", "/gametracker/view/DashUser.fxml");

        this.btnUserAddEntry.getStyleClass().add("dashboard-button");
        this.btnUserFindEntry.getStyleClass().add("dashboard-button");
        this.btnUserTodoList.getStyleClass().add("dashboard-button");
        this.btnUserGraphs.getStyleClass().add("dashboard-button");

        this.btnUserAddEntry.setText("ADICIONAR ENTRADA");
        this.btnUserFindEntry.setText("PESQUISAR ENTRADA");
        this.btnUserTodoList.setText("LISTA DE JOGOS PENDENTES");
        this.btnUserGraphs.setText("GRÁFICOS");

        this.btnUserAddEntry.setOnAction(event -> {
            onUserAddEntryClick();
        });
        this.btnUserFindEntry.setOnAction(event -> {
            onUserFindEntryClick();
        });
        this.btnUserTodoList.setOnAction(event -> {
            onUserTodoListClick();
        });
        this.btnUserGraphs.setOnAction(event -> {
            onUserGraphsClick();
        });
    }


    private void onUserAddEntryClick()
    {
        int platformCount = GameTrackerDAO.getInstance().countAllGamePlatform();
        
        if (platformCount <= 0)
        {
            if (GameTracker.getInstance().getLoggedPlayer().getPlayerPrivileges() ==
                    GAMEPLAYERPRIVILEGES.ADMIN)
            {
                GameTracker.getInstance().showErrorDialog(I18N.tr("UserDash.errorNoPlatforms.admin"));
            }
            else
            {
                GameTracker.getInstance().showErrorDialog(I18N.tr("UserDash.errorNoPlatforms.user"));
            }
            return;
        }
        
        GameTracker.getInstance().
                setMasterViewContent(new AddGameEntryController());
    }

    private void onUserFindEntryClick()
    {
        GameTracker.getInstance().
                setMasterViewContent(new UserFindEntryController());
    }

    private void onUserTodoListClick()
    {
        GameTracker.getInstance().
                setMasterViewContent(new UserTodoListController());
    }

    private void onUserGraphsClick()
    {
        GameTracker.getInstance().
                setMasterViewContent(new UserGraphsController());
    }

    @Override
    protected boolean hasErrorsInForm() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    protected void modelToView() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void viewToModel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}