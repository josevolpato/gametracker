/**
 * DashboardController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.util.ViewController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;


public class DashboardController extends ViewController
{
    private ToggleGroup groupDash;

    @FXML private ToggleButton tbtnUserDash;
    @FXML private ToggleButton tbtnPublicDash;
    @FXML private ToggleButton tbtnMasterDash;

    @FXML private AnchorPane apaneDash;


    public DashboardController()
    {
        super("DASHBOARD", "/gametracker/view/DashboardView.fxml");

        this.groupDash = new ToggleGroup();
        this.tbtnUserDash.setToggleGroup(groupDash);
        this.tbtnPublicDash.setToggleGroup(groupDash);
        this.tbtnMasterDash.setToggleGroup(groupDash);

        this.tbtnUserDash.setText("Jogador");
        this.tbtnPublicDash.setText("Público");
        this.tbtnMasterDash.setText("Master");

        this.tbtnUserDash.selectedProperty().addListener(
                new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                                Boolean oldValue, Boolean newValue)
            {
                if (newValue)
                {
                    showUserDash();
                }
            }

        });
        this.tbtnPublicDash.selectedProperty().addListener(
                new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                                Boolean oldValue, Boolean newValue)
            {
                if (newValue)
                {
                    showPublicDash();
                }
            }

        });
        this.tbtnMasterDash.selectedProperty().addListener(
                new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                                Boolean oldValue, Boolean newValue)
            {
                if (newValue)
                {
                    showMasterDash();
                }
            }

        });

        showUserDash();
    }


    public void showUserDash()
    {
        this.tbtnUserDash.setSelected(true);
        this.setDash(new DashUserController());
    }

    public void showPublicDash()
    {
        this.tbtnPublicDash.setSelected(true);
        this.setDash(new DashPublicController());
    }

    public void showMasterDash()
    {
        this.tbtnMasterDash.setSelected(true);
        this.setDash(new DashMasterController());
    }

    private void setDash(ViewController dashController)
    {
        this.apaneDash.getChildren().clear();
        this.apaneDash.getChildren().add(dashController.getParentNode());
    }
    
    @Override
    protected boolean hasErrorsInForm() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void modelToView() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void viewToModel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}