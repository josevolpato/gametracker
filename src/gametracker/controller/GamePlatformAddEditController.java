/**
 * GamePlatformAddEditController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.GameTracker;
import gametracker.util.ViewController;
import gametracker.model.GamePlatform;
import gametracker.util.GameTrackerDAO;
import gametracker.util.I18N;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Tela de criacao e edicao de GamePlatform.
 */
public final class GamePlatformAddEditController extends ViewController
{
    private GamePlatform gamePlatform;

    @FXML private Label lblName;
    @FXML private TextField txtName;
    @FXML private Button btnConfirm;


    public GamePlatformAddEditController()
    {
        super(I18N.tr("GamePlatformAddEditView.title.add"),
                "/gametracker/view/GamePlatformAddEditView.fxml");

        lblName.setText(I18N.tr("GamePlatformAddEditView.lblName"));
        btnConfirm.setText(I18N.tr("GamePlatformAddEditView.btnConfirm.add"));

        btnConfirm.setOnAction(event -> {
            addPlatform();
        });
        btnConfirm.setDefaultButton(true);

        gamePlatform = new GamePlatform();

        GameTracker.getInstance().setInitialFocus(txtName);
    }

    public GamePlatformAddEditController(GamePlatform platform)
    {
        super(I18N.tr("GamePlatformAddEditView.title.edit"),
                "/gametracker/view/GamePlatformAddEditView.fxml");

        lblName.setText(I18N.tr("GamePlatformAddEditView.lblName"));
        btnConfirm.setText(I18N.tr("GamePlatformAddEditView.btnConfirm.edit"));

        btnConfirm.setOnAction(event -> {
            editPlatform();
        });
        btnConfirm.setDefaultButton(true);

        gamePlatform = platform;
        modelToView();

        GameTracker.getInstance().setInitialFocus(txtName);
    }


    @Override
    protected boolean hasErrorsInForm()
    {
        if (txtName.getText().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlatformAddEditView.dialog.fillNameField"));
            return true;
        }
        
        return false;
    }

    @Override
    protected void modelToView()
    {
        txtName.setText(gamePlatform.getName());
    }

    @Override
    protected void viewToModel()
    {
        gamePlatform.setName(txtName.getText());
    }

    /**
     * Realiza a criacao de uma GamePlatform.
     */
    private void addPlatform()
    {
        if (hasErrorsInForm())
        {
            return;
        }

        viewToModel();

        if (GameTrackerDAO.getInstance().existsRepeatedGamePlatform(gamePlatform))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlatformAddEditView.dialog.platformAlreadyExists"));
            return;
        }
        
        GameTrackerDAO.getInstance().saveGamePlatform(gamePlatform);
        GameTracker.getInstance().showInformationDialog(I18N.tr("GamePlatformAddEditView.dialog.addGamePlatformSuccess"));
    }

    /**
     * Realiza a edicao de uma GamePlatform.
     */
    private void editPlatform()
    {
        if (hasErrorsInForm())
        {
            return;
        }

        viewToModel();
        
        if (GameTrackerDAO.getInstance().existsRepeatedGamePlatform(gamePlatform))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlatformAddEditView.dialog.platformAlreadyExists"));
            return;
        }
        
        GameTrackerDAO.getInstance().updateGamePlatform(gamePlatform);
        GameTracker.getInstance().showInformationDialog(I18N.tr("GamePlatformAddEditView.dialog.editGamePlatformSuccess"));
    }
}
