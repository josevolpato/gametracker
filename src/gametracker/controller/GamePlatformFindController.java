/**
 * FindPlatformController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.util.ViewController;
import gametracker.GameTracker;
import gametracker.model.GamePlatform;
import gametracker.util.GameTrackerDAO;
import gametracker.util.I18N;
import java.util.ArrayList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Define a tela de procura e gerenciamento de GamePlatform.
 */
public final class GamePlatformFindController extends ViewController
{
    @FXML private Label lblName;
    @FXML private TextField txtName;
    @FXML private Button btnFind;
    @FXML private TableView<GamePlatform> tablePlatform;
    @FXML private Button btnAddPlatform;
    @FXML private Button btnEditPlatform;
    @FXML private Button btnDeletePlatform;


    public GamePlatformFindController()
    {
        super(I18N.tr("GamePlatformFindView.title"),
                "/gametracker/view/GamePlatformFindView.fxml");

        lblName.setText(I18N.tr("GamePlatformFindView.lblName"));
        btnFind.setText(I18N.tr("GamePlatformFindView.btnFind"));
        btnAddPlatform.setText(I18N.tr("GamePlatformFindView.btnAddPlatform"));
        btnEditPlatform.setText(I18N.tr("GamePlatformFindView.btnEditPlatform"));
        btnDeletePlatform.setText(
                I18N.tr("GamePlatformFindView.btnDeletePlatform"));

        TableColumn columnId = new TableColumn(
                I18N.tr("GamePlatformFindView.tablePlatform.columnID"));
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnId.setPrefWidth(50.0);
        TableColumn columnName = new TableColumn(
                I18N.tr("GamePlatformFindView.tablePlatform.columnName"));
        columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnName.setPrefWidth(300.0);
        tablePlatform.getColumns().add(columnId);
        tablePlatform.getColumns().add(columnName);

        btnFind.setOnAction(event -> {
            findPlatform();
        });
        btnAddPlatform.setOnAction(event -> {
            addPlatform();
        });
        btnEditPlatform.setOnAction(event -> {
            editPlatform();
        });
        btnDeletePlatform.setOnAction(event -> {
            deletePlatform();
        });
        
        GameTracker.getInstance().setInitialFocus(txtName);
    }


    @Override
    protected boolean hasErrorsInForm() { return false; }

    @Override
    protected void modelToView() { }

    @Override
    protected void viewToModel() { }

    /**
     * Realiza a busca por GamePlatform segundo o formulario preenchido.
     */
    private void findPlatform()
    {
        tablePlatform.getItems().clear();
        ArrayList<GamePlatform> result = (ArrayList<GamePlatform>)
                GameTrackerDAO.getInstance().
                        searchGamePlatform(txtName.getText());

        if (result.size() <= 0)
        {
            GameTracker.getInstance().showInformationDialog(I18N.tr("GamePlatformFindView.dialog.findNoResult"));
            return;
        }
        
        tablePlatform.getItems().addAll(result);
    }

    /**
     * Redireciona para a tela de cadastro de GamePlatforms.
     */
    private void addPlatform()
    {
        GameTracker.getInstance().
                setMasterViewContent(new GamePlatformAddEditController());
    }

    /**
     * Redireciona para a tela de edicao de GamePlatforms.
     */
    private void editPlatform()
    {
        if (tablePlatform.getSelectionModel().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(
                    I18N.tr("GamePlatformFindView.editPlatformAlertDialog.noPlatformSelected"));
            return;
        }

        GameTracker.getInstance().setMasterViewContent(
                new GamePlatformAddEditController(
                        tablePlatform.getSelectionModel().getSelectedItem()));
    }

    /**
     * Realiza o processo de delete de uma GamePlatform.
     */
    private void deletePlatform()
    {
        if (tablePlatform.getSelectionModel().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(
                    I18N.tr("GamePlatformFindView.deletePlatformAlertDialog.noPlatformSelected"));
            return;
        }

        if (GameTracker.getInstance().showConfirmationDialog(
                I18N.tr("GamePlatformFindView.deletePlatformAlertDialog.deletionWarning")))
        {
            GameTrackerDAO.getInstance().
                    deleteGamePlatform(tablePlatform.getSelectionModel().getSelectedItem());
            tablePlatform.getItems().remove(
                    tablePlatform.getSelectionModel().getSelectedItem());
        }
    }
}
