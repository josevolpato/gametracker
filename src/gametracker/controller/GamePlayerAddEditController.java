/**
 * GamePlayerAddEditController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.GameTracker;
import gametracker.util.ViewController;
import gametracker.model.GamePlayer;
import gametracker.model.GamePlayerPrivilegesEnumConverter;
import gametracker.model.GamePlayerStatusEnumConverter;
import gametracker.util.GameTrackerDAO;
import gametracker.util.I18N;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * Define a tela de controle de usuarios do sistema pelo usuario master.
 */
public final class GamePlayerAddEditController extends ViewController
{
    private GamePlayer gamePlayer;

    @FXML private Label lblLogin;
    @FXML private TextField txtLogin;
    @FXML private Label lblEmail;
    @FXML private TextField txtEmail;
    @FXML private Label lblPassword;
    @FXML private PasswordField ptxtPassword;
    @FXML private Label lblPasswordRepeat;
    @FXML private PasswordField ptxtPasswordRepeat;
    @FXML private Label lblPlayerPrivileges;
    @FXML private ComboBox<String> cbPlayerPrivileges;
    @FXML private Label lblPlayerStatus;
    @FXML private ComboBox<String> cbPlayerStatus;
    @FXML private Button btnConfirm;


    public GamePlayerAddEditController()
    {
        super(I18N.tr("GamePlayerAddEditView.title.add"),
                "/gametracker/view/GamePlayerAddEditView.fxml");

        lblLogin.setText(I18N.tr("GamePlayerAddEditView.lblLogin"));
        lblEmail.setText(I18N.tr("GamePlayerAddEditView.lblEmail"));
        lblPassword.setText(I18N.tr("GamePlayerAddEditView.lblPassword"));
        lblPasswordRepeat.setText(I18N.tr("GamePlayerAddEditView.lblPasswordRepeat"));
        lblPlayerPrivileges.setText(I18N.tr("GamePlayerAddEditView.lblPlayerPrivileges"));
        lblPlayerStatus.setText(I18N.tr("GamePlayerAddEditView.lblPlayerStatus"));
        btnConfirm.setText(I18N.tr("GamePlayerAddEditView.btnConfirm.add"));

        cbPlayerPrivileges.getItems().addAll(GamePlayerPrivilegesEnumConverter.getInstance().getAllLiterals());
        cbPlayerStatus.getItems().addAll(GamePlayerStatusEnumConverter.getInstance().getAllLiterals());

        btnConfirm.setOnAction(ActionEvent -> {
            addGamePlayer();
        });
        btnConfirm.setDefaultButton(true);

        gamePlayer = new GamePlayer();

        GameTracker.getInstance().setInitialFocus(txtLogin);
    }

    public GamePlayerAddEditController(GamePlayer user)
    {
        super(I18N.tr("GamePlayerAddEditView.title.edit"),
                "/gametracker/view/GamePlayerAddEditView.fxml");

        lblLogin.setText(I18N.tr("GamePlayerAddEditView.lblLogin"));
        lblEmail.setText(I18N.tr("GamePlayerAddEditView.lblEmail"));
        lblPassword.setText(I18N.tr("GamePlayerAddEditView.lblPassword"));
        lblPasswordRepeat.setText(I18N.tr("GamePlayerAddEditView.lblPasswordRepeat"));
        lblPlayerPrivileges.setText(I18N.tr("GamePlayerAddEditView.lblPlayerPrivileges"));
        lblPlayerStatus.setText(I18N.tr("GamePlayerAddEditView.lblPlayerStatus"));
        btnConfirm.setText(I18N.tr("GamePlayerAddEditView.btnConfirm.edit"));

        cbPlayerPrivileges.getItems().addAll(GamePlayerPrivilegesEnumConverter.getInstance().getAllLiterals());
        cbPlayerStatus.getItems().addAll(GamePlayerStatusEnumConverter.getInstance().getAllLiterals());

        txtLogin.setDisable(true);

        btnConfirm.setOnAction(ActionEvent -> {
            editGamePlayer();
        });
        btnConfirm.setDefaultButton(true);

        gamePlayer = user;
        modelToView();

        GameTracker.getInstance().setInitialFocus(txtLogin);
    }


    @Override
    protected boolean hasErrorsInForm()
    {
        if (txtLogin.getText().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.fillLoginField"));
            return true;
        }

        if (txtEmail.getText().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.fillEmailField"));
            return true;
        }

        if (ptxtPassword.getText().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.fillPasswordField"));
            return true;
        }

        if (ptxtPasswordRepeat.getText().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.fillPasswordRepeatField"));
            return true;
        }

        if (!ptxtPassword.getText().equals(ptxtPasswordRepeat.getText()))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.passwordMismatch"));
            return true;
        }

        if (cbPlayerPrivileges.getSelectionModel().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.selectPlayerPrivileges"));
            return true;
        }

        if (cbPlayerStatus.getSelectionModel().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.selectPlayerStatus"));
            return true;
        }
        
        return false;
    }

    @Override
    protected void modelToView()
    {
        txtLogin.setText(gamePlayer.getLogin());
        txtEmail.setText(gamePlayer.getEmail());
        ptxtPassword.setText(gamePlayer.getPassword());
        ptxtPasswordRepeat.setText(gamePlayer.getPassword());
        cbPlayerPrivileges.getSelectionModel().select(
                GamePlayerPrivilegesEnumConverter.getInstance().
                        convertToLiteralValue(gamePlayer.getPlayerPrivileges()));
        cbPlayerStatus.getSelectionModel().select(
                GamePlayerStatusEnumConverter.getInstance().
                        convertToLiteralValue(gamePlayer.getPlayerStatus()));
    }

    @Override
    protected void viewToModel()
    {
        gamePlayer.setLogin(txtLogin.getText());
        gamePlayer.setEmail(txtEmail.getText());
        gamePlayer.setPassword(ptxtPassword.getText());
        gamePlayer.setPlayerPrivileges(GamePlayerPrivilegesEnumConverter.getInstance().
                convertToEnumValue(cbPlayerPrivileges.getSelectionModel().getSelectedItem()));
        gamePlayer.setPlayerStatus(GamePlayerStatusEnumConverter.getInstance().
                convertToEnumValue(cbPlayerStatus.getSelectionModel().getSelectedItem()));
    }

    /**
     *
     */
    private void addGamePlayer()
    {
        if (hasErrorsInForm())
        {
            return;
        }

        viewToModel();

        if (GameTrackerDAO.getInstance().existsRepeatedGamePlayer(gamePlayer))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.gamePlayerAlreadyExists"));
            return;
        }
        
        GameTrackerDAO.getInstance().saveGamePlayer(gamePlayer);
        GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.gamePlayerSaveSuccess"));
    }

    /**
     *
     */
    private void editGamePlayer()
    {
        if (hasErrorsInForm())
        {
            return;
        }

        viewToModel();

        if (GameTrackerDAO.getInstance().existsRepeatedGamePlayer(gamePlayer))
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.gamePlayerAlreadyExists"));
            return;
        }
        
        GameTrackerDAO.getInstance().updateGamePlayer(gamePlayer);
        GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerAddEditView.dialog.gamePlayerEditSuccess"));
    }
}
