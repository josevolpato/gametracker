/**
 * GamePlayerFindController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.util.ViewController;
import gametracker.GameTracker;
import gametracker.model.GamePlayer;
import gametracker.model.GamePlayerPrivilegesEnumConverter;
import gametracker.model.GamePlayerStatusEnumConverter;
import gametracker.util.GameTrackerDAO;
import gametracker.util.I18N;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

/**
 * Define a tela de procura e gerenciamento de GamePlayer.
 */
public final class GamePlayerFindController extends ViewController
{
    @FXML private Label lblLogin;
    @FXML private TextField txtLogin;
    @FXML private Label lblEmail;
    @FXML private TextField txtEmail;
    @FXML private Label lblPlayerPrivileges;
    @FXML private ComboBox<String> cbPlayerPrivileges;
    @FXML private Label lblPlayerStatus;
    @FXML private ComboBox<String> cbPlayerStatus;
    @FXML private Button btnFind;
    @FXML private TableView<GamePlayer> tableGamePlayer;
    @FXML private Button btnAddGamePlayer;
    @FXML private Button btnEditGamePlayer;
    @FXML private Button btnDeleteGamePlayer;


    public GamePlayerFindController()
    {
        super(I18N.tr("GamePlayerFindView.title"), "/gametracker/view/GamePlayerFindView.fxml");

        lblLogin.setText(I18N.tr("GamePlayerFindView.lblLogin"));
        lblEmail.setText(I18N.tr("GamePlayerFindView.lblEmail"));
        lblPlayerPrivileges.setText(I18N.tr("GamePlayerFindView.lblPlayerPrivileges"));
        cbPlayerPrivileges.getItems().add(I18N.tr("all"));
        cbPlayerPrivileges.getItems().addAll(GamePlayerPrivilegesEnumConverter.
                getInstance().getAllLiterals());
        cbPlayerPrivileges.getSelectionModel().select(0);
        lblPlayerStatus.setText(I18N.tr("GamePlayerFindView.lblPlayerStatus"));
        cbPlayerStatus.getItems().add(I18N.tr("all"));
        cbPlayerStatus.getItems().addAll(GamePlayerStatusEnumConverter.
                getInstance().getAllLiterals());
        cbPlayerStatus.getSelectionModel().select(0);
        btnFind.setText(I18N.tr("GamePlayerFindView.btnFind"));
        btnAddGamePlayer.setText(I18N.tr("GamePlayerFindView.btnAddGamePlayer"));
        btnEditGamePlayer.setText(I18N.tr("GamePlayerFindView.btnEditGamePlayer"));
        btnDeleteGamePlayer.setText(I18N.tr("GamePlayerFindView.btnDeleteGamePlayer"));

        TableColumn columnId = new TableColumn(I18N.tr("GamePlayerFindView.tableGamePlayer.columnId"));
        columnId.setPrefWidth(50.0);
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn columnLogin = new TableColumn(I18N.tr("GamePlayerFindView.tableGamePlayer.columnLogin"));
        columnLogin.setPrefWidth(300.0);
        columnLogin.setCellValueFactory(
                new PropertyValueFactory<>("login"));
        TableColumn columnEmail = new TableColumn(I18N.tr("GamePlayerFindView.tableGamePlayer.columnEmail"));
        columnEmail.setPrefWidth(300.0);
        columnEmail.setCellValueFactory(
                new PropertyValueFactory<>("email"));
        TableColumn columnPlayerPrivileges = new TableColumn(I18N.tr("GamePlayerFindView.tableGamePlayer.columnPlayerPrivileges"));
        columnPlayerPrivileges.setPrefWidth(150.0);
        columnPlayerPrivileges.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GamePlayer, Integer>,
                             ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(
                    TableColumn.CellDataFeatures<GamePlayer, Integer> param)
            {
                return new SimpleStringProperty(
                        GamePlayerPrivilegesEnumConverter.getInstance().convertToLiteralValue(param.getValue().
                                        getPlayerPrivileges()));
            }

        });
        TableColumn columnPlayerStatus = new TableColumn(I18N.tr("GamePlayerFindView.tableGamePlayer.columnPlayerStatus"));
        columnPlayerStatus.setPrefWidth(150.0);
        columnPlayerStatus.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GamePlayer, Integer>,
                             ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(
                    TableColumn.CellDataFeatures<GamePlayer, Integer> param)
            {
                return new SimpleStringProperty(
                        GamePlayerStatusEnumConverter.getInstance().convertToLiteralValue(param.getValue().getPlayerStatus()));
            }

        });
        tableGamePlayer.getColumns().add(columnId);
        tableGamePlayer.getColumns().add(columnLogin);
        tableGamePlayer.getColumns().add(columnPlayerPrivileges);
        tableGamePlayer.getColumns().add(columnPlayerStatus);

        btnFind.setOnAction(ActionEvent -> {
            findGamePlayer();
        });
        btnAddGamePlayer.setOnAction(ActionEvent -> {
            addGamePlayer();
        });
        btnEditGamePlayer.setOnAction(ActionEvent -> {
            editGamePlayer();
        });
        btnDeleteGamePlayer.setOnAction(ActionEvent -> {
            deleteGamePlayer();
        });

        GameTracker.getInstance().setInitialFocus(txtLogin);
    }


    @Override
    protected boolean hasErrorsInForm() { return false; }

    @Override
    protected void modelToView() { }

    @Override
    protected void viewToModel() { }

    /**
     * Realiza a busca por GamePlayer segundo o formulario preenchido.
     */
    private void findGamePlayer()
    {
        tableGamePlayer.getItems().clear();
        ArrayList<GamePlayer> result =
                (ArrayList<GamePlayer>) GameTrackerDAO.getInstance().
                        searchGamePlayer(txtLogin.getText(),
                                         txtEmail.getText(),
                                         GamePlayerPrivilegesEnumConverter.getInstance().
                                                 convertToEnumValue(cbPlayerPrivileges.getSelectionModel().getSelectedItem()),
                                         GamePlayerStatusEnumConverter.getInstance().
                                                 convertToEnumValue(cbPlayerStatus.getSelectionModel().getSelectedItem()));

        if (result.size() > 0)
        {
            tableGamePlayer.getItems().addAll(result);
        }
    }

    /**
     * Redireciona para a tela de cadastro de GamePlayer.
     */
    private void addGamePlayer()
    {
        GameTracker.getInstance().
                setMasterViewContent(new GamePlayerAddEditController());
    }

    /**
     * Redireciona para a tela de edicao de GamePlayer.
     */
    private void editGamePlayer()
    {
        if (tableGamePlayer.getSelectionModel().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerFindView.editGamePlayerAlertDialog.noUserSelected"));
            return;
        }

        GameTracker.getInstance().setMasterViewContent(
                new GamePlayerAddEditController(tableGamePlayer.getSelectionModel().
                        getSelectedItem()));
    }

    /**
     * Realiza o processo de delete de um GamePlayer.
     */
    private void deleteGamePlayer()
    {
        if (tableGamePlayer.getSelectionModel().isEmpty())
        {
            GameTracker.getInstance().showAlertDialog(I18N.tr("GamePlayerFindView.deleteGamePlayerAlertDialog.noUserSelected"));
            return;
        }

        if (GameTracker.getInstance().showConfirmationDialog(I18N.tr("GamePlayerFindView.deleteGamePlayerAlertDialog.deletionWarning")))
        {
            GameTrackerDAO.getInstance().
                    deleteGamePlayer(tableGamePlayer.getSelectionModel().getSelectedItem());
            tableGamePlayer.getItems().remove(
                    tableGamePlayer.getSelectionModel().getSelectedItem());
        }
    }
}
