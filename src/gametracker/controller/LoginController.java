/**
 * LoginController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.util.ViewController;
import gametracker.GameTracker;
import gametracker.component.ComboBoxFlagButtonCell;
import gametracker.component.ComboBoxFlagCell;
import gametracker.model.GamePlayer;
import gametracker.util.GameTrackerDAO;
import gametracker.util.I18N;
import gametracker.util.LanguageEnumConverter;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import gametracker.util.DynamicalyTranslatableViewController;

/**
 * LoginController
 * 
 * Define a classe de controle da tela de login e suas funcionalidades.
 */
public class LoginController extends ViewController implements DynamicalyTranslatableViewController
{
    @FXML private ImageView imgLogo;
    @FXML private ComboBox<String> cbLanguage;
    @FXML private Label lblLogin;
    @FXML private TextField txtLogin;
    @FXML private Label lblPassword;
    @FXML private TextField ptxtPassword;
    @FXML private Label lblNotification;
    @FXML private Button btnConfirm;
    @FXML private Hyperlink lkCreateGamePlayer;
    

    public LoginController()
    {
        super(I18N.tr("LoginView.title"), "/gametracker/view/LoginView.fxml");
        
        cbLanguage.setButtonCell(new ComboBoxFlagButtonCell());
        cbLanguage.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {

            @Override
            public ListCell<String> call(ListView<String> param)
            {
                return new ComboBoxFlagCell();
            }

        });
        cbLanguage.getItems().addAll(LanguageEnumConverter.getInstance().getAllLiterals());
        cbLanguage.getSelectionModel().select(LanguageEnumConverter.getInstance().convertToLiteralValue(GameTracker.getInstance().getLanguage()));
        cbLanguage.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event)
            {
                GameTracker.getInstance().setLanguage(LanguageEnumConverter.getInstance().convertToEnumValue(cbLanguage.getSelectionModel().getSelectedItem()));
            }
            
        });
        
        lblNotification.setVisible(false);
        
        btnConfirm.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event)
            {
                onLoginClick();
            }
            
        });
        btnConfirm.setDefaultButton(true);
        
        lblLogin.setText(I18N.tr("LoginView.lblLogin"));
        lblPassword.setText(I18N.tr("LoginView.lblPassword"));
        btnConfirm.setText(I18N.tr("LoginView.btnConfirm"));
        lkCreateGamePlayer.setText(I18N.tr("LoginView.lkCreateGamePlayer"));
        
        lkCreateGamePlayer.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event)
            {
                onCreateAccountClick();
            }
            
        });
        
        GameTracker.getInstance().setInitialFocus(txtLogin);
    }
    
    
    @Override
    protected void modelToView() { }

    @Override
    protected void viewToModel() { }
    
    @Override
    protected boolean hasErrorsInForm()
    {
        if (txtLogin.getText().equals(""))
        {
            lblNotification.setText(I18N.tr("LoginView.lblNotification.fillUserField"));
            lblNotification.setVisible(true);
            return true;
        }

        if (ptxtPassword.getText().equals(""))
        {
            lblNotification.setText(I18N.tr("LoginView.lblNotification.fillPasswordField"));
            lblNotification.setVisible(true);
            return true;
        }

        return false;
    }
    
    @Override
    public final void updateTranslation()
    {
        lblLogin.setText(I18N.tr("LoginView.lblLogin"));
        lblPassword.setText(I18N.tr("LoginView.lblPassword"));
        btnConfirm.setText(I18N.tr("LoginView.btnConfirm"));
        lkCreateGamePlayer.setText(I18N.tr("LoginView.lkCreateGamePlayer"));
    }
    
    /**
     * Metodo executado quando o botao de login e clicado.
     */
    public void onLoginClick()
    {
        lblNotification.setVisible(false);

        if (hasErrorsInForm())
        {
            return;
        }

        GamePlayer gamePlayer = GameTrackerDAO.getInstance().login(txtLogin.getText(),
                ptxtPassword.getText());
        if (gamePlayer == null)
        {
            lblNotification.setText(I18N.tr("LoginView.lblNotification.errorLogin"));
            lblNotification.setVisible(true);
            return;
        }

        GameTracker.getInstance().doLogin(gamePlayer);
    }
    
    public void onCreateAccountClick()
    {
        GameTracker.getInstance().setMainView(new CreateAccountController());
    }
}
