/**
 * MasterController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.util.ViewController;
import gametracker.GameTracker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import gametracker.util.DynamicalyTranslatableViewController;


public class MasterController extends ViewController implements DynamicalyTranslatableViewController
{
    private ViewController masterContentController;

    @FXML private ImageView imgAppLogo;
    @FXML private Label lblViewName;
    @FXML private Label lblLoggedUser;
    @FXML private Button btnConfigurationView;
    @FXML private Button btnHelpView;
    @FXML private Button btnBackView;
    @FXML private AnchorPane apaneMasterView;


    public MasterController()
    {
       super("", "/gametracker/view/MasterView.fxml");

       btnBackView.setOnAction((ActionEvent event) -> {
           GameTracker.getInstance().onBackPressed();
       });

    }


    public void setMasterViewContent(ViewController viewController)
    {
        this.masterContentController = viewController;
        this.lblViewName.setText(viewController.getViewTitle());
        this.apaneMasterView.getChildren().clear();
        AnchorPane.setTopAnchor(viewController.getParentNode(), 0.0);
        AnchorPane.setLeftAnchor(viewController.getParentNode(), 0.0);
        AnchorPane.setRightAnchor(viewController.getParentNode(), 0.0);
        AnchorPane.setBottomAnchor(viewController.getParentNode(), 0.0);
        this.apaneMasterView.getChildren().add(viewController.getParentNode());
    }

    public ViewController getMasterViewController()
    {
        return this.masterContentController;
    }

    public void onLogoutClick()
    {
        GameTracker.getInstance().doLogout();
    }

    public void setBackViewButtonVisible(boolean visibility)
    {
        this.btnBackView.setVisible(visibility);
    }

    public void setBackViewButtonDisabled(boolean disable)
    {
        this.btnBackView.setDisable(disable);
    }

    @Override
    protected boolean hasErrorsInForm() { return false; }
    
    @Override
    protected void modelToView() { }

    @Override
    protected void viewToModel() { }

    @Override
    public void updateTranslation()
    {
        if (masterContentController instanceof DynamicalyTranslatableViewController)
        {
            ((DynamicalyTranslatableViewController) masterContentController).updateTranslation();
        }
    }

}