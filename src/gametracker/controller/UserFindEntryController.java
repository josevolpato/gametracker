/**
 * UserFindEntryController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.controller;

import gametracker.util.ViewController;
import gametracker.model.GameEntryStatusEnumConverter;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;


public class UserFindEntryController extends ViewController
{
    @FXML private Label lblGameName;
    @FXML private TextField txtGameName;
    @FXML private Label lblPlatform;
    @FXML private ComboBox<String> cbPlatform;
    @FXML private Label lblEntryStatus;
    @FXML private ComboBox<String> cbEntryStatus;
    @FXML private Label lblStillPlaying;
    @FXML private ComboBox<String> cbStillPlaying;
    @FXML private Button btnFind;
    @FXML private Label lblTableResult;
    @FXML private TableView tableResult;


    public UserFindEntryController()
    {
        super("Procurar entrada", "/gametracker/view/UserFindEntryView.fxml");

        lblGameName.setText("Nome:");
        lblPlatform.setText("Plataforma:");
        lblEntryStatus.setText("Status:");
        lblStillPlaying.setText("Ainda jogando?");
        btnFind.setText("Pesquisar");
        lblTableResult.setText("Resultados:");

        cbEntryStatus.getItems().addAll(GameEntryStatusEnumConverter.
                getInstance().getAllLiterals());
        cbStillPlaying.getItems().add("SIM");
        cbStillPlaying.getItems().add("NÃO");

    }

    @FXML
    private void find()
    {
        if (txtGameName.getText().isEmpty() &&
            cbPlatform.getSelectionModel().isEmpty() &&
            cbEntryStatus.getSelectionModel().isEmpty() &&
            cbStillPlaying.getSelectionModel().isEmpty())
        {
            // Deve ter ao menos um campo preenchido
            return;
        }

        // Envia a query para o banco

    }
    
    @Override
    protected boolean hasErrorsInForm() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void modelToView() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void viewToModel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
