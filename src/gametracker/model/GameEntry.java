/**
 * GameEntry.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Define o modelo de entrada de registro de jogos.
 */
@Entity
@Table(name = "gameentry")
public class GameEntry implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "gamename", nullable = false)
    private String gameName;

    @OneToOne
    @JoinColumn(name = "gameplatformid", nullable = false)
    private GamePlatform gamePlatform;

    @OneToOne
    @JoinColumn(name = "gameplayerid", nullable = false)
    private GamePlayer gamePlayer;

    @Column(name = "entrystatus", nullable = false)
    private GAMEENTRYSTATUS entryStatus;

    @Column(name = "stillplaying", nullable = false)
    private boolean stillPlaying;

    @Column(name = "comment", nullable = true)
    private String comment;


    public GameEntry() { }

    public GameEntry(String gameName,
                     GamePlatform gamePlatform,
                     GamePlayer gamePlayer,
                     GAMEENTRYSTATUS entryStatus,
                     boolean stillPlaying,
                     String comment)
    {
        this.gameName = gameName;
        this.gamePlatform = gamePlatform;
        this.gamePlayer = gamePlayer;
        this.entryStatus = entryStatus;
        this.stillPlaying = stillPlaying;
        this.comment = comment;
    }


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getGameName()
    {
        return gameName;
    }

    public void setGameName(String name)
    {
        this.gameName = name;
    }

    public GamePlatform getGamePlatform()
    {
        return gamePlatform;
    }

    public void setGamePlatform(GamePlatform gamePlatform)
    {
        this.gamePlatform = gamePlatform;
    }

    public GamePlayer getGamePlayer()
    {
        return gamePlayer;
    }

    public void setGamePlayer(GamePlayer gamePlayer)
    {
        this.gamePlayer = gamePlayer;
    }

    public GAMEENTRYSTATUS getEntryStatus()
    {
        return this.entryStatus;
    }

    public void setEntryStatus(GAMEENTRYSTATUS entryStatus)
    {
        this.entryStatus = entryStatus;
    }

    public boolean isStillPlaying()
    {
        return stillPlaying;
    }

    public void setStillPlaying(boolean stillPlaying)
    {
        this.stillPlaying = stillPlaying;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }
}
