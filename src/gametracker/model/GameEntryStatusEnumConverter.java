/**
 * GameEntryStatusEnumConverter.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeConverter;
import gametracker.util.EnumLiteralConverter;
import gametracker.util.I18N;

/**
 * Define uma classe de conversao para GAMEENTRYSTATUS.
 */
public class GameEntryStatusEnumConverter
        implements EnumLiteralConverter<GAMEENTRYSTATUS>,
                   AttributeConverter<GAMEENTRYSTATUS, Integer>
{
    private static GameEntryStatusEnumConverter instance;

    public static GameEntryStatusEnumConverter getInstance()
    {
        if (instance == null)
        {
            instance = new GameEntryStatusEnumConverter();
        }
        return instance;
    }


    @Override
    public GAMEENTRYSTATUS convertToEnumValue(String comboboxValue)
    {
        if (comboboxValue.equals(I18N.tr("GameEntryStatus.unfinished")))
        {
            return GAMEENTRYSTATUS.UNFINISHED;
        }

        if (comboboxValue.equals(I18N.tr("GameEntryStatus.finished")))
        {
            return GAMEENTRYSTATUS.FINISHED;
        }

        if (comboboxValue.equals(I18N.tr("GameEntryStatus.completed")))
        {
            return GAMEENTRYSTATUS.COMPLETED;
        }

        return GAMEENTRYSTATUS.INVALID;
    }

    @Override
    public String convertToLiteralValue(GAMEENTRYSTATUS enumValue)
    {
        switch (enumValue)
        {
            case UNFINISHED: return I18N.tr("GameEntryStatus.unfinished");

            case FINISHED: return I18N.tr("GameEntryStatus.finished");

            case COMPLETED: return I18N.tr("GameEntryStatus.completed");

            default: return I18N.tr("invalid");
        }
    }

    @Override
    public List<GAMEENTRYSTATUS> getAllValues()
    {
        List<GAMEENTRYSTATUS> allValues = new ArrayList<>();
        allValues.add(GAMEENTRYSTATUS.UNFINISHED);
        allValues.add(GAMEENTRYSTATUS.FINISHED);
        allValues.add(GAMEENTRYSTATUS.COMPLETED);
        return allValues;
    }

    @Override
    public List<String> getAllLiterals()
    {
        List<String> allLiterals = new ArrayList<>();
        allLiterals.add(convertToLiteralValue(GAMEENTRYSTATUS.UNFINISHED));
        allLiterals.add(convertToLiteralValue(GAMEENTRYSTATUS.FINISHED));
        allLiterals.add(convertToLiteralValue(GAMEENTRYSTATUS.COMPLETED));
        return allLiterals;
    }

    @Override
    public Integer convertToDatabaseColumn(GAMEENTRYSTATUS enumValue)
    {
        switch (enumValue)
        {
            case UNFINISHED: return 1;

            case FINISHED: return 2;

            case COMPLETED: return 3;

            default: return 0;
        }
    }

    @Override
    public GAMEENTRYSTATUS convertToEntityAttribute(Integer databaseValue)
    {
        switch (databaseValue)
        {
            case 1: return GAMEENTRYSTATUS.UNFINISHED;

            case 2: return GAMEENTRYSTATUS.FINISHED;

            case 3: return GAMEENTRYSTATUS.COMPLETED;

            default: return GAMEENTRYSTATUS.INVALID;
        }
    }
}
