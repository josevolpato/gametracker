/**
 * GamePlayer.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Define um jogador, que tambem e um usuario do sistema, que pode-se associar a
 * entradas de registro de jogos.
 */
@Entity
@Table(name = "gameplayer")
public class GamePlayer implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password; // TODO encriptar a senha

    @Column(name = "playerprivileges", nullable = false)
    private GAMEPLAYERPRIVILEGES playerPrivileges;

    @Column(name = "playerstatus", nullable = false)
    private GAMEPLAYERSTATUS playerStatus;


    public GamePlayer()
    {
        this("",
             "",
             "",
             GAMEPLAYERPRIVILEGES.USER,
             GAMEPLAYERSTATUS.WAITING_ACTIVATION);
    }

    public GamePlayer(String login,
                      String email,
                      String password,
                      GAMEPLAYERPRIVILEGES playerPrivileges,
                      GAMEPLAYERSTATUS playerStatus)
    {
        super();
        this.login = login;
        this.email = email;
        this.password = password;
        this.playerPrivileges = playerPrivileges;
        this.playerStatus = playerStatus;
    }


    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPassword()
    {
        return this.password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public GAMEPLAYERPRIVILEGES getPlayerPrivileges()
    {
        return playerPrivileges;
    }

    public void setPlayerPrivileges(GAMEPLAYERPRIVILEGES playerPrivileges)
    {
        this.playerPrivileges = playerPrivileges;
    }

    public GAMEPLAYERSTATUS getPlayerStatus()
    {
        return playerStatus;
    }

    public void setPlayerStatus(GAMEPLAYERSTATUS playerStatus)
    {
        this.playerStatus = playerStatus;
    }
}
