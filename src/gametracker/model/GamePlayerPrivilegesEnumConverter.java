/**
 * GamePlayerPrivilegesEnumConverter.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeConverter;
import gametracker.util.EnumLiteralConverter;
import gametracker.util.I18N;

/**
 * Define uma classe de conversao para GAMEPLAYERPRIVILEGES.
 */
public class GamePlayerPrivilegesEnumConverter
        implements EnumLiteralConverter<GAMEPLAYERPRIVILEGES>,
                   AttributeConverter<GAMEPLAYERPRIVILEGES, Integer>
{
    private static GamePlayerPrivilegesEnumConverter instance;

    public static GamePlayerPrivilegesEnumConverter getInstance()
    {
        if (instance == null)
        {
            instance = new GamePlayerPrivilegesEnumConverter();
        }
        return instance;
    }


    @Override
    public GAMEPLAYERPRIVILEGES convertToEnumValue(String literalValue)
    {
        if (literalValue.equals(I18N.tr("GamePlayerPrivileges.player")))
        {
            return GAMEPLAYERPRIVILEGES.USER;
        }

        if (literalValue.equals(I18N.tr("GamePlayerPrivileges.admin")))
        {
            return GAMEPLAYERPRIVILEGES.ADMIN;
        }

        return GAMEPLAYERPRIVILEGES.INVALID;
    }

    @Override
    public List<GAMEPLAYERPRIVILEGES> getAllValues()
    {
        List<GAMEPLAYERPRIVILEGES> allValues = new ArrayList<>();
        allValues.add(GAMEPLAYERPRIVILEGES.USER);
        allValues.add(GAMEPLAYERPRIVILEGES.ADMIN);
        return allValues;
    }

    @Override
    public String convertToLiteralValue(GAMEPLAYERPRIVILEGES enumValue)
    {
        switch (enumValue)
        {
            case USER:
                return I18N.tr("GamePlayerPrivileges.player");

            case ADMIN:
                return I18N.tr("GamePlayerPrivileges.admin");

            default:
                return I18N.tr("invalid");
        }
    }

    @Override
    public List<String> getAllLiterals()
    {
        List<String> allLiterals = new ArrayList<>();
        allLiterals.add(convertToLiteralValue(GAMEPLAYERPRIVILEGES.USER));
        allLiterals.add(convertToLiteralValue(GAMEPLAYERPRIVILEGES.ADMIN));
        return allLiterals;
    }

    @Override
    public Integer convertToDatabaseColumn(GAMEPLAYERPRIVILEGES enumValue)
    {
        switch (enumValue)
        {
            case USER: return 1;

            case ADMIN: return 2;

            default: return 0;
        }
    }

    @Override
    public GAMEPLAYERPRIVILEGES convertToEntityAttribute(Integer databaseValue)
    {
        switch (databaseValue)
        {
            case 1: return GAMEPLAYERPRIVILEGES.USER;

            case 2: return GAMEPLAYERPRIVILEGES.ADMIN;

            default: return GAMEPLAYERPRIVILEGES.INVALID;
        }
    }
}
