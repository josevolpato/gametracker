/**
 * GamePlayerStatusEnumConverter.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.model;

import gametracker.util.EnumLiteralConverter;
import gametracker.util.I18N;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeConverter;

/**
 * Define uma classe de conversao para GAMEPLAYERSTATUS.
 */
public class GamePlayerStatusEnumConverter
        implements EnumLiteralConverter<GAMEPLAYERSTATUS>,
                   AttributeConverter<GAMEPLAYERSTATUS, Integer>
{
    private static GamePlayerStatusEnumConverter instance;

    public static GamePlayerStatusEnumConverter getInstance()
    {
        if (instance == null)
        {
            instance = new GamePlayerStatusEnumConverter();
        }
        return instance;
    }


    @Override
    public GAMEPLAYERSTATUS convertToEnumValue(String literalValue)
    {
        if (literalValue.equals(I18N.tr("GamePlayerStatus.waitingActivation")))
        {
            return GAMEPLAYERSTATUS.WAITING_ACTIVATION;
        }

        if (literalValue.equals(I18N.tr("GamePlayerStatus.active")))
        {
            return GAMEPLAYERSTATUS.ACTIVE;
        }

        if (literalValue.equals(I18N.tr("GamePlayerStatus.banned")))
        {
            return GAMEPLAYERSTATUS.BANNED;
        }

        return GAMEPLAYERSTATUS.INVALID;
    }

    @Override
    public String convertToLiteralValue(GAMEPLAYERSTATUS enumValue)
    {
        switch (enumValue)
        {
            case WAITING_ACTIVATION: return I18N.tr("GamePlayerStatus.waitingActivation");

            case ACTIVE: return I18N.tr("GamePlayerStatus.active");

            case BANNED: return I18N.tr("GamePlayerStatus.banned");

            default: return I18N.tr("GamePlayerStatus.unfinished");
        }
    }

    @Override
    public List<GAMEPLAYERSTATUS> getAllValues()
    {
        List<GAMEPLAYERSTATUS> allValues = new ArrayList<>();
        allValues.add(GAMEPLAYERSTATUS.WAITING_ACTIVATION);
        allValues.add(GAMEPLAYERSTATUS.ACTIVE);
        allValues.add(GAMEPLAYERSTATUS.BANNED);
        return allValues;
    }

    @Override
    public List<String> getAllLiterals()
    {
        List<String> allLiterals = new ArrayList<>();
        allLiterals.add(convertToLiteralValue(GAMEPLAYERSTATUS.WAITING_ACTIVATION));
        allLiterals.add(convertToLiteralValue(GAMEPLAYERSTATUS.ACTIVE));
        allLiterals.add(convertToLiteralValue(GAMEPLAYERSTATUS.BANNED));
        return allLiterals;
    }

    @Override
    public Integer convertToDatabaseColumn(GAMEPLAYERSTATUS enumValue)
    {
        switch (enumValue)
        {
            case WAITING_ACTIVATION: return 1;

            case ACTIVE: return 2;

            case BANNED: return 3;

            default: return 0;
        }
    }

    @Override
    public GAMEPLAYERSTATUS convertToEntityAttribute(Integer databaseValue)
    {
        switch (databaseValue)
        {
            case 1: return GAMEPLAYERSTATUS.WAITING_ACTIVATION;

            case 2: return GAMEPLAYERSTATUS.ACTIVE;

            case 3: return GAMEPLAYERSTATUS.BANNED;

            default: return GAMEPLAYERSTATUS.INVALID;
        }
    }
}
