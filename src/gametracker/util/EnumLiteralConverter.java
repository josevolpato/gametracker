/**
 * EnumLiteralConverter.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.util;

import java.util.List;

/**
 * Classe auxiliar que realiza a conversao de um valor enum para seu valor
 * correspondente literal (String) e vice-versa.
 * @param <T> Tipo do enum
 */
public interface EnumLiteralConverter<T extends Enum>
{
    /**
     * Converte o valor literal para o valor enum correspondente.
     *
     * @param literalValue Valor literal a ser convertido
     *
     * @return Valor enum correspondente
     */
    public T convertToEnumValue(String literalValue);

    /**
     * Converte o valor enum para o valor literal correspondente.
     *
     * @param enumValue Valor enum a ser convertido
     *
     * @return Valor literal correspondente
     */
    public String convertToLiteralValue(T enumValue);

    /**
     * Retorna todos os valores enum definidos.
     *
     * @return Lista contendo todos os valores enum definidos.
     */
    public List<T> getAllValues();

    /**
     * Retorna todos os valores literais definidos.
     *
     * @return Lista contendo todos os valores literais definidos.
     */
    public List<String> getAllLiterals();
}
