/**
 * GameTrackerDAO.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.util;

import gametracker.model.GAMEENTRYSTATUS;
import gametracker.model.GAMEPLAYERPRIVILEGES;
import gametracker.model.GAMEPLAYERSTATUS;
import gametracker.model.GameEntry;
import gametracker.model.GamePlatform;
import gametracker.model.GamePlayer;
import gametracker.model.GamePlayerPrivilegesEnumConverter;
import gametracker.model.GamePlayerStatusEnumConverter;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

/**
 *
 */
public class GameTrackerDAO
{
    private static GameTrackerDAO instance;

    private SessionFactory factory;
    private Session session;
    private Transaction transaction;

    private GameTrackerDAO()
    {
        Configuration configuration = new Configuration();

        configuration
            .setProperty("hibernate.dialect", "org.hibernate.dialect.SQLiteDialect")
            .setProperty("hibernate.connection.driver_class", "org.sqlite.JDBC")
            .setProperty("hibernate.connection.url", "jdbc:sqlite:gametracker.db")
            .setProperty("hibernate.connection.username", "")
            .setProperty("hibernate.connection.password", "")
            .setProperty("hibernate.hbm2ddl.auto", "update")
            .addAnnotatedClass(GamePlayer.class)
            .addAnnotatedClass(GamePlatform.class)
            .addAnnotatedClass(GameEntry.class);

        factory = configuration.buildSessionFactory();
        session = factory.openSession();
    }

    public static GameTrackerDAO getInstance()
    {
        if (instance == null)
        {
            instance = new GameTrackerDAO();
        }
        return instance;
    }


    public void setUpDatabase()
    {

    }

    public GamePlayer login(String login, String password)
    {
        Query query = session.createQuery("FROM GamePlayer WHERE login = :login AND password = :password");
        query.setParameter("login", login);
        query.setParameter("password", password);
        List<GamePlayer> result = query.list();
        if (result == null || result.size() <= 0)
        {
            return null;
        }
        return result.get(0);
    }
    
    public boolean existsRepeatedGamePlayer(GamePlayer gamePlayer)
    {
        Query query = session.createQuery("SELECT COUNT(*) FROM GamePlayer WHERE login = :login");
        query.setParameter("login", gamePlayer.getLogin());
        Long gamePlayers = (Long) query.uniqueResult();
        return gamePlayers > 0;
    }

    public void saveGamePlayer(GamePlayer gamePlayer)
    {
        transaction = session.beginTransaction();
        session.save(gamePlayer);
        transaction.commit();
    }

    public void updateGamePlayer(GamePlayer gamePlayer)
    {
        transaction = session.beginTransaction();
        session.update(gamePlayer);
        transaction.commit();
    }

    public void deleteGamePlayer(GamePlayer gamePlayer)
    {
        transaction = session.beginTransaction();
        session.delete(gamePlayer);
        transaction.commit();
    }

    public List<GamePlayer> searchGamePlayer(String login,
                                             String email,
                                             GAMEPLAYERPRIVILEGES playerPrivileges,
                                             GAMEPLAYERSTATUS playerStatus)
    {
        StringBuilder where = new StringBuilder();

        if (login != null && !login.equals(""))
        {
            where.append(" login LIKE :login");
        }

        if (email != null && !email.equals(""))
        {
            if (where.length() > 0)
            {
                where.append(" AND");
            }
            where.append(" email LIKE :email");
        }

        if (playerPrivileges != null && playerPrivileges != GAMEPLAYERPRIVILEGES.INVALID)
        {
            if (where.length() > 0)
            {
                where.append(" AND");
            }
            where.append(" playerprivileges = :playerprivileges");
        }

        if (playerStatus != null && playerStatus != GAMEPLAYERSTATUS.INVALID)
        {
            if (where.length() > 0)
            {
                where.append(" AND");
            }
            where.append(" playerstatus = :playerstatus");
        }

        StringBuilder sql = new StringBuilder();
        sql.append("FROM GamePlayer");

        if (where.length() > 0)
        {
            sql.append(" WHERE");
            sql.append(where);
        }

        Query query = session.createQuery(sql.toString());
        if (login != null && !login.equals(""))
        {
            query.setParameter("login", "%" + login + "%");
        }

        if (email != null && !email.equals(""))
        {
            query.setParameter("email", "%" + email + "%");
        }

        if (playerPrivileges != null && playerPrivileges != GAMEPLAYERPRIVILEGES.INVALID)
        {
            query.setParameter("playerprivileges", 
                    GamePlayerPrivilegesEnumConverter.getInstance().
                            convertToDatabaseColumn(playerPrivileges));
        }

        if (playerStatus != null && playerStatus != GAMEPLAYERSTATUS.INVALID)
        {
            query.setParameter("playerstatus", 
                    GamePlayerStatusEnumConverter.getInstance().
                            convertToDatabaseColumn(playerStatus));
        }

        return query.list();
    }
    
    public boolean existsRepeatedGamePlatform(GamePlatform gamePlatform)
    {
        Query query = session.createQuery("SELECT COUNT(*) FROM GamePlatform WHERE name = :name");
        query.setParameter("name", gamePlatform.getName());
        Long gamePlatforms = (Long) query.uniqueResult();
        return gamePlatforms > 0;
    }

    public void saveGamePlatform(GamePlatform gamePlatform)
    {
        transaction = session.beginTransaction();
        session.save(gamePlatform);
        transaction.commit();
    }

    public void updateGamePlatform(GamePlatform gamePlatform)
    {
        transaction = session.beginTransaction();
        session.update(gamePlatform);
        transaction.commit();
    }

    public void deleteGamePlatform(GamePlatform gamePlatform)
    {
        transaction = session.beginTransaction();
        session.delete(gamePlatform);
        transaction.commit();
    }

    public List<GamePlatform> searchGamePlatform(String name)
    {
        StringBuilder where = new StringBuilder();

        if (name != null && !name.equals(""))
        {
            where.append(" name LIKE :name");
        }

        StringBuilder sql = new StringBuilder();
        sql.append("FROM GamePlatform");

        if (where.length() > 0)
        {
            sql.append(" WHERE");
            sql.append(where);
        }

        Query query = session.createQuery(sql.toString());
        if (name != null && !name.equals(""))
        {
            query.setParameter("name", "%" + name + "%");
        }

        return query.list();
    }

    public List<GamePlatform> getAllGamePlatform()
    {
        return searchGamePlatform("");
    }

    public int countAllGamePlatform()
    {
        Query query = session.createQuery("SELECT COUNT(*) FROM GamePlatform");
        return ((Long) query.uniqueResult()).intValue();
    }

    public DAO_SAVE_RESULT saveGameEntry(GameEntry gameEntry)
    {
        Query query = session.createQuery("FROM GameEntry WHERE gamename = :gamename");
        query.setParameter("gamename", gameEntry.getGameName());
        List gamePlatforms = query.list();
        if (gamePlatforms.size() > 0)
        {
            return DAO_SAVE_RESULT.ID_ALREADY_ON_TABLE;
        }

        transaction = session.beginTransaction();
        session.save(gameEntry);
        transaction.commit();
        return DAO_SAVE_RESULT.SUCCESS;
    }

    public void updateGameEntry(GameEntry gameEntry)
    {
        transaction = session.beginTransaction();
        session.update(gameEntry);
        transaction.commit();
    }

    public void deleteGameEntry(GameEntry gameEntry)
    {
        transaction = session.beginTransaction();
        session.delete(gameEntry);
        transaction.commit();
    }

    public List<GameEntry> searchGameEntry(String gameName,
                                           int gamePlatformId,
                                           GAMEENTRYSTATUS entryStatus,
                                           Boolean stillPlaying)
    {
        StringBuilder where = new StringBuilder();

        if (gameName != null && !gameName.equals(""))
        {
            where.append(" gamename LIKE :gamename");
        }

        if (gamePlatformId > 0)
        {
            if (where.length() > 0)
            {
                where.append(" AND");
            }
            where.append(" gameplatformid = :gameplatformid");
        }

        if (entryStatus != null && entryStatus != GAMEENTRYSTATUS.INVALID)
        {
            if (where.length() > 0)
            {
                where.append(" AND");
            }
            where.append(" entrystatus = :entrystatus");
        }

        if (stillPlaying != null)
        {
            if (where.length() > 0)
            {
                where.append(" AND");
            }
            where.append(" stillplaying = :stillplaying");
        }

        StringBuilder sql = new StringBuilder();
        sql.append("FROM GameEntry");

        if (where.length() > 0)
        {
            sql.append(" WHERE");
            sql.append(where);
        }

        Query query = session.createQuery(sql.toString());
        if (gameName != null && !gameName.equals(""))
        {
            query.setParameter("gamename", "%" + gameName + "%");
        }

        if (gamePlatformId > 0)
        {
            query.setParameter("gameplatformid", gamePlatformId);
        }

        if (entryStatus != null && entryStatus == GAMEENTRYSTATUS.INVALID)
        {
            query.setParameter("entrystatus", entryStatus);
        }

        if (stillPlaying != null)
        {
            query.setParameter("stillplaying", stillPlaying);
        }

        return query.list();
    }

    public void closeDatabase()
    {
        session.close();
        factory.close();
    }
}
