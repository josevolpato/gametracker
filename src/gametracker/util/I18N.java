/**
 * I18N.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.util;

import gametracker.GameTracker;
import java.util.ResourceBundle;

/**
 * Classe responsavel pela internacionalizacao do programa.
 */
public final class I18N
{
    private static ResourceBundle bundle;
    
    /**
     * Substitui um valor chave pela String internacionalizada correspondente.
     * Metodo de conveniencia.
     * 
     * @param key - Valor chave de uma String
     * 
     * @return String internacionalizada correspondente.
     */
    public static String tr(String key)
    {
        return translate(key);
    }
    
    /**
     * Substitui um valor chave pela String internacionalizada correspondente.
     * 
     * @param key - Valor chave de uma String
     * 
     * @return String internacionalizada correspondente.
     */
    public static String translate(String key)
    {
        bundle = ResourceBundle.getBundle("gametracker/languages/gametracker",
                GameTracker.getInstance().getLocale());
        return bundle.getString(key);
    }
}
