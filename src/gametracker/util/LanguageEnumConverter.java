/**
 * LanguageEnumConverter.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Define uma classe de conversao de enum LANGUAGE.
 */
public class LanguageEnumConverter implements EnumLiteralConverter<LANGUAGE>
{
    private static LanguageEnumConverter instance;

    private LanguageEnumConverter() { }


    public static LanguageEnumConverter getInstance()
    {
        if (instance == null)
        {
            instance = new LanguageEnumConverter();
        }
        return instance;
    }

    @Override
    public LANGUAGE convertToEnumValue(String literalValue)
    {
        if (literalValue.equals(I18N.tr("Languages.portuguese")))
        {
            return LANGUAGE.PORTUGUESE;
        }

        if (literalValue.equals(I18N.tr("Languages.english")))
        {
            return LANGUAGE.ENGLISH;
        }

        if (literalValue.equals(I18N.tr("Languages.spanish")))
        {
            return LANGUAGE.SPANISH;
        }

        return LANGUAGE.INVALID;
    }

    @Override
    public String convertToLiteralValue(LANGUAGE enumValue)
    {
        switch (enumValue)
        {
            case PORTUGUESE: return I18N.tr("Languages.portuguese");

            case ENGLISH: return I18N.tr("Languages.english");

            case SPANISH: return I18N.tr("Languages.spanish");

            default: return I18N.tr("invalid");
        }
    }

    @Override
    public List<LANGUAGE> getAllValues()
    {
        List<LANGUAGE> allValues = new ArrayList<>();
        allValues.add(LANGUAGE.PORTUGUESE);
        allValues.add(LANGUAGE.ENGLISH);
        allValues.add(LANGUAGE.SPANISH);
        return allValues;
    }

    @Override
    public List<String> getAllLiterals()
    {
        List<String> allValues = new ArrayList<>();
        allValues.add(I18N.tr("Languages.portuguese"));
        allValues.add(I18N.tr("Languages.english"));
        allValues.add(I18N.tr("Languages.spanish"));
        return allValues;
    }

}
