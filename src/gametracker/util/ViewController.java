/**
 * ViewController.java
 *
 * Copyright (c) 2016 - 2018 José Volpato
 *
 * This file is part of Gametracker.
 *
 * Gametracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gametracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gametracker. If not, see <http://www.gnu.org/licenses/>.
 */
package gametracker.util;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

/**
 * Define a classe base de uma tela do programa.
 */
public abstract class ViewController
{
    private String viewTitle;
    private Parent parentNode;


    public ViewController(String viewTitle, String fxmlpath)
    {
        this.viewTitle = viewTitle;

        FXMLLoader fxmlLoader =
                new FXMLLoader(getClass().getResource(fxmlpath));
        try {
            fxmlLoader.setController(this);
            parentNode = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(
                    ViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Retorna o titulo da tela.
     * 
     * @return Titulo da tela.
     */
    public String getViewTitle()
    {
        return this.viewTitle;
    }

    /**
     * Retorna o elemento pai de todos os elementos da tela.
     * 
     * @return Elemento pai de todos os elementos da tela.
     */
    public Parent getParentNode()
    {
        return this.parentNode;
    }

    /**
     * Metodo que faz a verificacao de erros em um formulario da tela.
     * 
     * @return 
     * true - Ha erros no formulario.
     * false - Nao ha erros no formulario.
     */
    protected abstract boolean hasErrorsInForm();
    
    /**
     * Metodo responsavel pela transmissao de dados do modelo para o formulario 
     * da tela.
     */
    protected abstract void modelToView();

    /**
     * Metodo responsavel pela transmissao de dados do formulario da tela para o
     * modelo.
     */
    protected abstract void viewToModel();
}
